# Docker & Kubernetes

This example shows how to use Docker, and Docker Compose, for local development workflow with Slim Framework; as well as a setup that can be used to deploy this application to Kubernetes.

While this examples uses Slim Framework, it should be trivial to modify for most other PHP frameworks.

Kubernetes as a local development environment will be covered [here](../kubernetes).
